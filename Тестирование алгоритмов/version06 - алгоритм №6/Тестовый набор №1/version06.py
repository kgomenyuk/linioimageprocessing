import pytesseract
import cv2
import numpy as np
import re
import pymysql
from PIL import Image

#transform string
def transform(str):
    reg = re.compile('[^а-яА-я]')
    str = reg.sub('', str)
    str = str.lower()
    return str

#сalculates the Levenshtein distance between a and b
def distance(a, b):
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a, b = b, a
        n, m = m, n

    current_row = range(n+1) # Keep current and previous row, not entire matrix
    for i in range(1, m+1):
        previous_row, current_row = current_row, [i]+[0]*n
        for j in range(1,n+1):
            add, delete, change = previous_row[j]+1, current_row[j-1]+1, previous_row[j-1]
            if a[j-1] != b[i-1]:
                change += 1
            current_row[j] = min(add, delete, change)
    return current_row[n]

#retrive name of station from database
def retrive_db():
    conn = pymysql.connect(host='127.0.0.1', user='root', passwd='231995', db='metro', charset='utf8')
    cur = conn.cursor()
    cur.execute("SELECT * FROM station")
    data = cur.fetchall()
    station = []
    for i in range(len(data)):
        station.append([])
        station[i].append(data[i][0])
        station[i].append(data[i][1])
    cur.close()
    conn.close()
    return(station)

comment = 'version06 - алгоритм №6 - тестовый набор №1'
number_running = 1
ocr = []

for j in range(1, 81, 1): #ПОПРАВИТЬ на (1, 81, 1)
    #ReadFile
    fileName = 'station (' + str(j) + ').jpg'
    image = cv2.imread('station/station (%s).jpg' %str(j))

    #gayscaling
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    cv2.imwrite('grayscaling/station (%s).png' %str(j), image)

    #medianfilter
    image = cv2.medianBlur(image, 3)
    cv2.imwrite('medianfilter/station (%s).png' %str(j), image)

    #binarization
    image = cv2.convertScaleAbs(image)
    image = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
    cv2.imwrite('binarization/station (%s).png' %str(j), image)

    cv2.imwrite('temp.png', image)
    image = Image.open('temp.png')
    image.load()

    #OCR
    text = pytesseract.image_to_string(image, lang='rus')
    ocr.append([])
    ocr[j-1].append(j)
    ocr[j-1].append(text)
    ocr[j-1].append(fileName)

#получение словаря
conn = pymysql.connect(host='127.0.0.1', user='root', passwd='231995', db='metro', charset='utf8')
cur = conn.cursor()
cur.execute("SELECT * FROM station ORDER BY id")
data = cur.fetchall()
station = []
for i in range(len(data)):
    station.append([])
    station[i].append(data[i][0])
    station[i].append(data[i][1])
cur.close()
conn.close()

#получение эталона
conn = pymysql.connect(host='127.0.0.1', user='root', passwd='231995', db='metro', charset='utf8')
cur = conn.cursor()
cur.execute("SELECT * FROM reference ORDER BY id_pk")
data = cur.fetchall()
reference = []
for i in range(len(data)):
    reference.append([])
    reference[i].append(data[i][0])
    reference[i].append(data[i][1])
    reference[i].append(data[i][2])
    reference[i].append(data[i][3])
cur.close()
conn.close()

for j in range(len(station)):
    station[j][1] = transform(station[j][1])
for j in range(len(ocr)):
    ocr[j][1] = transform(ocr[j][1])

result = []

for i in range(len(ocr)):
    minLevenshtein = distance(ocr[i][1], station[1][1]) #min Levenshtein distance
    probableStation = station[1][0]
    probableStationName = station[1][1]
    comparison = 0
    for k in range(len(station)):
        dist = distance(ocr[i][1], station[k][1])
        if minLevenshtein > dist:
            minLevenshtein = dist
            probableStation = station[k][0]
            probableStationName = station[k][1]
    if probableStation == reference[i][2]:
        comparison = 1
    result.append([])
    result[i].append(comparison)
    result[i].append(probableStation)
    result[i].append(probableStationName)
    result[i].append(reference[i][2])
    result[i].append(reference[i][3])
    result[i].append(ocr[i][2])
    result[i].append(comment)
    result[i].append(ocr[i][1])
    result[i].append(number_running)

#write into database
conn = pymysql.connect(host='127.0.0.1', user='root', passwd='231995', db='metro', charset='utf8')
cur = conn.cursor()
for i in range(len(result)):
    add_result = ("INSERT INTO metro.new_result "
              "(`result`, `id_station`, `name_station`, `rigth_id_station`, `right_name_station`, `file_name`, `comment`, `ocr`, `number_running`) "
              "VALUES (%(result)s, %(id_station)s, %(name_station)s, %(rigth_id_station)s, %(right_name_station)s, %(file_name)s, %(comment)s, %(ocr)s, %(number_running)s)")
    data_result = {
        'result': result[i][0],
        'id_station': result[i][1],
        'name_station': result[i][2],
        'rigth_id_station': result[i][3],
        'right_name_station': result[i][4],
        'file_name': result[i][5],
        'comment': result[i][6],
        'ocr': result[i][7],
        'number_running': result[i][8],
    }
    cur.execute(add_result, data_result)
conn.commit()
cur.close()
conn.close()

print(result)

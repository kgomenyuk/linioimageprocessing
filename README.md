Репозиторий предназначен для хранения и распространения созданных в результате выполнения исследовательской работы исходных кодов программного компонента, наборов тестовых данных, детальных результаты тестирования, а также справочных таблиц.

Папка "Тестовые данные" содержит в себе:
* 2 набора тестовых данных;
* Справочную таблицу (словарь) по станциям московского метрополитена в виде ("id" - идентификатор станции в БД; "name" - наименование станции);
* Таблицу эталонных значений для сопоставления элементов наборов тестовых данных со справочной таблицей в виде ("file_name" - название файла из тестового набора; "id_station" - идентификатор соответствующей записи из справочной таблицы; "name_station" - название станции, соответствующей файлу).

Папка "Тестирование алгоритмов" содержит в себе:
* Таблицу детальных результатов тестирования в виде ("result" - верно ли была распознана станция с изображения, возможные значения: 0 - нет, 1 - да; "id_station" - идентификатор записи справочной таблицы, которой соответствует распознанная алгоритмом станция; "name_station" - название распознанной станции; "right_id_station" - эталонное значение идентификатора записи справочной таблицы для обрабатываемого изображения; "right_name_station" - эталонное название станции с изображения; "file_name" - название обрабатываемого файла из тестового набора; "comment" - комментарий, формирующий по маске <название файла-исходника реализующего алгоритм>' - '<название алгоритма>' - '<название обрабатываемого тестового набора>; "ocr" - искомая строка).
* Папки с исходными кодами тестируемых алгоритмов.